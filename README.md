# Neovim Plugin Template

This project is intended to serve as both a template for the creation of personal Neovim plugins,
and as a personal wiki where I can compile notes describing how Neovim plugins are structured.

# Including your Plugin into Neovim

## Packer

In Packer you can include your plugin via the local system path, or your Github/Gitlab URL.

```
use "/home/user/path/to/template.nvim"
```

```
use "jtyler76/template.nvim"
```

# Project Structure

## lua Folder

Within your project folder, you'll need to include a `lua` folder. This is where you'll store your
lua code, and is a folder that Neovim will recognize and include.

You can include/run a `lua` script as follows:
```
:lua require("path/to/lua/file")
```

Alternatively, you can have a folder within the `lua` folder, called `foo` for example, and if you
include a `init.lua` script within it, you can include/run that `init.lua` script by requiring the
folder.
```
:lua require("foo")
```

## plugin Folder

Another important folder within your project is the `plugin` folder. Any `lua` files residing in
this folder will be executed on startup.


# Lua Module Best Practice

Commonly `lua` scripts in a Neovim plugins will define a table that functions are added to, and then
that table is returned as a handle for the caller to access the scripts functions with.

You could for example define a lua module called `foo.lua` as follows:
```lua foo.lua
local M = {}

M.bar = function()
    -- do stuff
end

return M
```

And then you could require and use the module as follows:
```vim
:lua foo = require("foo")
:lua foo.bar()
```






<!-- vim: set tw=100 : -->
